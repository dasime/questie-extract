import csv
import os
from argparse import ArgumentParser
from datetime import datetime
from typing import Any, List, Tuple

import pytz
from slpp import slpp as lua  # type: ignore


def main() -> None:
    parser = ArgumentParser(
        description="Extract character history from Questie's database to CSV."
    )

    group = parser.add_mutually_exclusive_group(required=True)

    group.add_argument(
        "-w",
        "--wow-dir",
        help="Path to the directory containing WowClassic.exe.",
        type=str,
        default=None,
    )

    group.add_argument(
        "-q",
        "--questie-path",
        help="Path to Questie.lua.",
        type=str,
        default=None,
    )

    parser.add_argument(
        "--tz",
        help="Server Timezone (Defaults to: Europe/Paris)",
        type=str,
        default="Europe/Paris",
    )

    parser.add_argument(
        "--iso8601",
        help="Format timestamps in ISO-8601 (YYYY-MM-DDTHH:MM:SS+hh:mm). Useful for further processing in other software.",
        action="store_true",
        default=False,
    )

    options = parser.parse_args()

    tz = pytz.timezone(options.tz)

    if options.wow_dir:
        for account_name, path in crawl_wow_path(options.wow_dir):
            print(f"[{account_name}] Account detected")
            if not path:
                print("--Questie.lua not found")
                break
            process_questie(path, tz, options.iso8601)

    if options.questie_path:
        print(f"Directly accessing '{options.questie_path}'")
        try:
            os.stat(options.questie_path)
        except FileNotFoundError:
            print(f"{options.questie_path} doesn't appear to be a file")
            exit(1)

        process_questie(options.questie_path, tz, options.iso8601)


def crawl_wow_path(wow_dir: str) -> List[Tuple[str, str]]:
    detected_questie_paths = []

    try:
        os.stat(os.path.join(wow_dir, "WowClassic.exe"))
    except FileNotFoundError:
        print("'WowClassic.exe' not found, are you sure you're in '_classic_'?")
        exit(1)

    account_path = os.path.join(wow_dir, "WTF/Account")
    for file in os.scandir(account_path):
        if file.is_file():
            break

        if file.name == "SavedVariables":
            break

        questie_path = os.path.join(
            account_path, file.name, "SavedVariables/Questie.lua"
        )
        try:
            os.stat(questie_path)
        except FileNotFoundError:
            questie_path = ""

        detected_questie_paths.append((file.name, questie_path))

    return detected_questie_paths


def process_questie(questie_path: str, tz: Any, iso8601: bool) -> None:
    try:
        history = read_questie(questie_path)
    except KeyError:
        print(f"--'{questie_path}' doesn't appear to contain Questie data")
        return

    for character in history["char"]:
        print(f"--Writing history for {character}")
        write_journey(character, history["char"][character]["journey"], tz, iso8601)


def read_questie(questie_path: str) -> dict:
    with open(questie_path) as questie_lua:
        parsed = lua.decode(f"{{{questie_lua.read()}}}")

        return parsed["QuestieConfig"]


def write_journey(character_name: str, journey: dict, tz: Any, iso8601: bool) -> None:
    with open(f"{character_name}.csv", "w", newline="") as csvfile:
        header = [
            "Timestamp",
            "Event",
            "Level",
            "Quest ID",
            "Quest Status",
            "Party",
        ]

        writer = csv.DictWriter(csvfile, fieldnames=header)

        writer.writeheader()

        for event in journey:
            writer.writerow(
                {
                    "Timestamp": format_date(event["Timestamp"], tz, iso8601),
                    "Event": event["Event"],
                    "Level": event.get("Level", event.get("NewLevel", "ERROR")),
                    "Quest ID": event.get("Quest"),
                    "Quest Status": event.get("SubType"),
                    "Party": ", ".join(
                        [
                            f"{member['Name']} ({member['Level']})"
                            for member in event.get("Party", [])
                        ]
                    ),
                }
            )


def format_date(timestamp: str, tz: Any, iso8601: bool) -> str:
    date = datetime.fromtimestamp(timestamp, tz)

    if iso8601:
        return date.isoformat()

    return date.strftime("%Y-%m-%d %H:%M:%S")


if __name__ == "__main__":
    main()
