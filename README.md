# Questie Extract

Extract character history from [Questie](https://github.com/Questie/Questie)'s database to CSV.

## Installation

Requires [Python](https://www.python.org/).

```shell
pip install questie-extract --extra-index-url https://gitlab.com/api/v4/projects/29367392/packages/pypi/simple
```

## Usage

_All examples below are almost ready to paste into Window's cmd.exe._
_They just need the path correcting for your computer._

Either point `questie-extract` to the directory containing `WowClassic.exe`
and it will extract data from `Questie.lua` for every account in that directory:

```shell
questie-extract ^
    -w "F:\World of Warcraft\_classic_"
```
```
[DASIME] Account detected
--Writing history for Fui - Mograine
--Writing history for Tl - Mograine
[Nobody] Account detected
--Questie.lua not found
```

Or point `questie-extract` to a specific `Questie.lua` and it'll extract just
character data for that account:

```shell
questie-extract ^
    -q "F:\World of Warcraft\_classic_\WTF\Account\DASIME\SavedVariables\Questie.lua"
```
```
Directly accessing 'F:\World of Warcraft\_classic_\WTF\Account\DASIME\SavedVariables\Questie.lua'
--Writing history for Fui - Mograine
--Writing history for Tl - Mograine
```

If you're _NOT_ playing on an EU server you also need to specify the timezone of
the server you play on to get the timestamps correctly parsed.

```shell
questie-extract ^
    -q "F:\World of Warcraft\_classic_\WTF\Account\DASIME\SavedVariables\Questie.lua" ^
    --tz US/Eastern
```

`questie-extract` defaults to `Europe/Paris` which all EU servers are on.

### Optional Expert Options

The default timestamp format is `YYYY-MM-DD HH:MM:SS` of the **server time**.
This is good for spreadsheets but not very good for further processing because
relative server time can vary with daylights savings.

To output timestamps in `YYYY-MM-DDTHH:MM:SS+hh:mm` use the `--iso8601` option:

```shell
questie-extract ^
    -q "F:\World of Warcraft\_classic_\WTF\Account\DASIME\SavedVariables\Questie.lua"
```
```
Timestamp           , Event , Level , Quest ID , Quest Status , Party
2019-08-27 01:12:51 , Quest , 1     , 179      , Accept       ,
2019-08-27 01:17:31 , Level , 2     ,          ,              ,
```
```shell
questie-extract ^
    -q "F:\World of Warcraft\_classic_\WTF\Account\DASIME\SavedVariables\Questie.lua" ^
    --iso8601
```
```

Timestamp                 , Event , Level , Quest ID , Quest Status , Party
2019-08-27T01:12:51+02:00 , Quest , 1     , 179      , Accept       ,
2019-08-27T01:17:31+02:00 , Level , 2     ,          ,              ,
```

## Data Exploring

The program generates one CSV for the journey log of each character:

```
Timestamp           , Event , Level , Quest ID , Quest Status , Party
2019-08-27 01:12:51 , Quest , 1     , 179      , Accept       ,
2019-08-27 01:17:31 , Level , 2     ,          ,              ,
2019-08-27 01:18:12 , Quest , 2     , 179      , Complete     ,
2019-08-27 01:18:13 , Quest , 2     , 233      , Accept       ,
2019-08-27 01:22:38 , Quest , 2     , 233      , Complete     ,
```

Party information is included where available as "Name (level)":

```
Timestamp           , Event , Level , Quest ID , Quest Status , Party
2019-12-28 22:20:19 , Level , 3     ,          ,              , "Zui (3) , Cui (3)"
2019-12-28 22:20:20 , Quest , 3     , 459      , Complete     , "Zui (3) , Cui (3)"
2019-12-28 22:20:41 , Quest , 3     , 916      , Accept       , "Zui (3) , Cui (3)"
```

The Quest number can easily be looked up on WoWHead like this:

* `Quest ID: 179` - <https://classic.wowhead.com/quest=179/>
* `Quest ID: 233` - <https://classic.wowhead.com/quest=233/>
* `Quest ID: 459` - <https://classic.wowhead.com/quest=459/>
* `Quest ID: 916` - <https://classic.wowhead.com/quest=916/>

The easiest way to do something with the CSV is to open it with your
favourite spreadsheet software.
Just select the first row and hit AutoFilter to get easy filtering.

Interesting things to query:

* Reduce the `Events` column to just `Level` to get a the time of each ding
* Reduce the `Quest Status` column to just `Abandoned` to see all the quests
  you dropped
* Search the `Party` column for your friend's character to see what events
  they were with you for
* Reduce the `Level` column to just `60` and `Quest Status` to just `Complete`
  to see only the quests you completed after hitting max level

## TODO

* It should be possible to resolve the quest name locally.
  Questie has three `QuestDB.lua` files containing a large mapping of ID to Quest:

  - <https://github.com/Questie/Questie/blob/master/ExternalScripts(DONOTINCLUDEINRELEASE)/questDB.lua>
  - <https://github.com/Questie/Questie/blob/master/Database/Classic/classicQuestDB.lua>
  - <https://github.com/Questie/Questie/blob/master/Database/TBC/tbcQuestDB.lua>

  It's also possible to pull this from somewhere like WoWHead.

## History

This was originally started at the start of May 2021 when I was making room on
my games SSD.

1. I first wrote a script in LUA for going from `Questie.lua` to JSON.
2. Then I rewrote it in Javascript because Nodejs is easier to distribute.
3. Then I realised I wanted to play with the data more so rewrote it in Python
   using Pandas, but that just made me realise other people might want to play
   with their data and Pandas is not very user friendly.
4. Finally I removed Pandas and output to CSV. The data set is small enough
   you can easily play and manipulate it in a spreadsheet and [people really
   just want Excel](https://twitter.com/RemyPorter/status/752913168624746497).

Then it sat on my computer for a few months annoying me because I didn't
push it for anybody else to use. Until I had a weekend at the start of
September where I actually needed to do something and that was the motivation
needed!
